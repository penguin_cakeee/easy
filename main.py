import easyocr


def test(file):
    reader = easyocr.Reader(['ru'])
    res = reader.detect(file)
    #res2 = reader.recognize(file, horizontal_list=[[409, 910, 51, 107]], free_list=[])
    res2 = reader.readtext(file, blocklist='_,/|{}[];*+єIqwrYuUiSsDdfFQgGhJjklLzZVvNn', mag_ratio=2.0, detail=False,
                           contrast_ths=0.4, adjust_contrast=0.7, height_ths=0.9, ycenter_ths=0.9, width_ths=1)
    return res2


def fu(s):
    d = {'выписка из протокола': s[1] + ' ' + s[2], 'дата': s[4], 'номер': s[6], 'пункт': s[8],
         'наименование объекта': s[10] + ' ' + s[11], 'авторы проекта': s[13],
         'генеральная проектная организация': s[17] + ' ' + s[18], 'застройщик': s[20],
         'расмотрение на рабочей комиссии': '-', 'референт': s[24] + ' ' + s[25], 'докладчик': '-', 'выступили': '-'}
    return d


def main():
    file = '2.jpg'
    s = test(file=file)
    print(fu(s))


if __name__ == "__main__":
    main()


